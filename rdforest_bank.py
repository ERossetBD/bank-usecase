import mlflow
import mlflow.sklearn
import pandas as pd
import sys

from sklearn.model_selection import train_test_split 
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics

from mlflow.models.signature import infer_signature

if __name__ == "__main__":

	data = pd.read_csv('BankChurners.csv')
	data.drop(columns=["CLIENTNUM","Naive_Bayes_Classifier_Attrition_Flag_Card_Category_Contacts_Count_12_mon_Dependent_count_Education_Level_Months_Inactive_12_mon_1", "Naive_Bayes_Classifier_Attrition_Flag_Card_Category_Contacts_Count_12_mon_Dependent_count_Education_Level_Months_Inactive_12_mon_2"],inplace=True)

	data_encoded = pd.get_dummies(data.drop(columns=['Attrition_Flag']))
	data_encoded['Attrition_Flag'] = data['Attrition_Flag']
	
	with mlflow.start_run():
	
		n_estimators = int(sys.argv[1])
		max_depth = int(sys.argv[2])
		max_features = int(sys.argv[3])
		test_size = float(sys.argv[4])
		
		dataTrain, dataTest = train_test_split(data_encoded,test_size=test_size,stratify=data_encoded.Attrition_Flag)
		
		rf = RandomForestClassifier(n_estimators = n_estimators, max_depth = max_depth, max_features = max_features)
		rf.fit(dataTrain.iloc[:,:-1], dataTrain.Attrition_Flag)

		pred = rf.predict(X=dataTest.iloc[:,:-1])

		accuracy = metrics.accuracy_score(dataTest.Attrition_Flag,pred)
		f1_score = metrics.f1_score(dataTest.Attrition_Flag,pred,pos_label="Existing Customer")
		auc_score = metrics.roc_auc_score(dataTest.Attrition_Flag,rf.predict_proba(X=dataTest.iloc[:,:-1])[:,1])
		
		signature = infer_signature(dataTrain, pred)
		
		mlflow.sklearn.log_model(rf, "model", signature=signature)
		mlflow.log_param("n_estimators", n_estimators)
		mlflow.log_param("max_depth", max_depth)
		mlflow.log_param("max_features", max_features)
		mlflow.log_param("test_size", test_size)
		mlflow.log_metric("accuracy", accuracy)
		mlflow.log_metric("f1_score", f1_score)
		mlflow.log_metric("auc_score", auc_score)
		mlflow.log_artifact("BankChurners.csv")

		print("accuracy: " + str(accuracy))
		print("f1_score: " + str(f1_score))
		print("auc_score: " + str(auc_score))